package com.aplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.aplication.entitys.MenuCategory;


public interface MenuCategoryRepository extends CrudRepository<MenuCategory,Integer> {
  
	  @Query(value = "select * from menu_category where category_name = ?1 and state = 0", nativeQuery = true)
	  MenuCategory findByName(String name);
	  
	  @Query(value = "select * from menu_category where state = 0", nativeQuery = true)
	  List<MenuCategory> findAllActive();
}
